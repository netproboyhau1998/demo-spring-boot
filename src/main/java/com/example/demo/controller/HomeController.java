package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {
	
	@PostMapping(value = "/quan-tri")
	public String homePage() {
		return "web/home";

	}
	
	@GetMapping(value = "/quan-tri/trang-chu")
	public String adminPage() {
		return "views/admin/home";
	}
}
